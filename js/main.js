"use strict"
// 1) структурное представление документа 
// 2) innerHTML возвращает вложенную HTML разметку в виде текста, 
//    а innerText текст находящийся внутри елемента
// 3) для этого сушествует несколько методов getElementById(),getElementByTagName(),
//    ,getElementByClassName() и querySelector(). 
//    Для меня предпочтительным являеться querySelector().

for(let parag of document.querySelectorAll("p")){
    parag.style.backgroundColor = "#ff0000"
}

console.log(document.querySelector("#optionsList"));
console.log(document.querySelector("#optionsList").parentElement);
if (document.querySelector("#optionsList").hasChildNodes()) {
    for (const item of document.querySelector("#optionsList").children) {
        console.log(item.nodeType, item.nodeName);
    }
}

document.querySelector("#testParagraph").innerHTML = "<p>This is a paragraph</p>";


for(let item of document.querySelectorAll(".main-header li")){
    item.classList.add("nav-item");
    console.log(item);
}
for(let item of document.querySelectorAll(".section-title")){
    item.classList.remove("section-title")
    console.log(item);
}

